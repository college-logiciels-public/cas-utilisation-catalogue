# Je dois remonter des infos sur les logiciels pour les indicateurs

## Méthode
Ces *personas* sont des portraits robots d'utilisateurs fictifs mais réalistes.
Il ne s'agit donc pas de décrire des utilisateurs idéaux mais des situations réalistes.
Qu'est-ce qui peut encourager ou freiner l'utilisation d'un catalogue de logiciels? 

Le 1er enjeu est de se concentrer sur :
- **le type de tâches que doivent réaliser ces acteurs**: avec quels outils travaillent-ils ? avec quels autres acteurs ? y quelles sont les tâches récurrentes ? Quelles sont les tâches les plus importantes ? Quelles tâches souhaiteraient-ils réaliser différemment ? 
- **leurs questionnements** : quand le choix est possible, quels sont leurs critères de décision pour adopter un outil ? Quand il n'y a pas le choix, qui détermine l'outil à utiliser ? 


## Description de la situation

Dans mon laboratoire, je suis préposé à la fourniture de listes et d'informations sur les logiciels (c'est la vie). 

Lorsque je crée un nouveau logiciel, je veux bien remplir une fiche, mais pas autant de fiches qu'il y a de tutelles.
Quand je publie une mise à jour du logiciel dans la base de données, je ne veux pas avoir à remplir de nouveau la fiche de A à Z.

Je ne devrais avoir qu'un clic à faire pour indiquer la nouvelle version ; le reste devrait être automatisé. Idéalement, les informations de la fiche sont "commitées" dans le dépôt de mon logiciel au fur et à mesure (e.g. ajout des auteurs), prêtes à être intégrées dans le catalogue lorsque la nouvelle version est publiée.

Mais pour le moment, le processus est essentiellement manuel (c'est la vie - bis). 
Un autre souci : dans le cas d'homonymes, il est parfois difficile de savoir qui a fait quoi. Je remplis les champs mais il n'y a pas de lien avec un identifiant par exemple. Sans une fine connaissance des axes de recherche des uns et des autres, il peut être très difficile d'attribuer la bonne responsabilité intellectuelle. 

Un autre problème est qu'on me sollicite parfois pour fournir des listes qui doivent tenir compte de certains critères. 
J'ai mes méthodes pour identifier des logiciels selon des besoins scientifiques, mais il me manque un outil pour effectuer des recherches fines avec des critères de licence, etc.


