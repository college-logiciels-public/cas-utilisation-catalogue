# Cas d'usage concernant les chercheurs et enseignants-chercheurs, les doctorants

- [1: je cherche un logiciel pour un besoin particulier](https://gricad-gitlab.univ-grenoble-alpes.fr/college-logiciels-public/cas-utilisation-catalogue/-/blob/main/Chercheurs_EnseignantsChercheurs/UC1.md)
- [2: je développe un logiciel que je veux rendre visible](https://gricad-gitlab.univ-grenoble-alpes.fr/college-logiciels-public/cas-utilisation-catalogue/-/blob/main/Chercheurs_EnseignantsChercheurs/UC2.md)
- [3:  je contribue (ou contribuerais) à un logiciel existant plutôt
  que repartir de
  zéro](https://gricad-gitlab.univ-grenoble-alpes.fr/college-logiciels-public/cas-utilisation-catalogue/-/blob/main/Chercheurs_EnseignantsChercheurs/UC3.md)
- [4: J'ai des données dans un format spécifique et je cherche un logiciel](https://gricad-gitlab.univ-grenoble-alpes.fr/college-logiciels-public/cas-utilisation-catalogue/-/blob/main/Chercheurs_EnseignantsChercheurs/UC4.md)
