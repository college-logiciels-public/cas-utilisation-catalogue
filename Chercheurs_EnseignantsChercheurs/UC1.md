# Je cherche un logiciel pour un besoin particulier

## Méthode
Ces *personas* sont des portraits robots d'utilisateurs fictifs mais réalistes.
Il ne s'agit donc pas de décrire des utilisateurs idéaux mais des situations réalistes.
Qu'est-ce qui peut encourager ou freiner l'utilisation d'un catalogue de logiciels? 

Le 1er enjeu est de se concentrer sur :
- **le type de tâches que doivent réaliser ces acteurs**: avec quels outils travaillent-ils ? avec quels autres acteurs ? y quelles sont les tâches récurrentes ? Quelles sont les tâches les plus importantes ? Quelles tâches souhaiteraient-ils réaliser différemment ? 
- **leurs questionnements** : quand le choix est possible, quels sont leurs critères de décision pour adopter un outil ? Quand il n'y a pas le choix, qui détermine l'outil à utiliser ? 


## Description de la situation

J'aborde une nouvelle question scientifique. J'ai fait pas mal de recherche biblio : il y a des personnes qui ont travaillé sur le domaine et qui ont publié.
Comme mon modèle est très proche, plutôt que de tout recoder, je vais reprendre un code existant.

### Splendeur et décadence du bouche à oreille
Un collègue m'a dit qu'il y avait un logiciel fait à l'université Bidule, qui faisait tel truc. Mais je ne me souviens plus du nom, j'ai du mal à le retrouver. Le collègue n'en était plus sûr non plus. 
A défaut, je voudrais au moins le contact de quelqu'un qui aurait un zip du logiciel sur son ordinateur ou l'url dans le Gitlab interne du labo. 

Je prie pour qu'il ne s'agisse pas d'un logiciel développé dans le cadre de contrat de recherche par des non-permanents, doctorant.e.s, post-doc, etc. 
Non pas que ces logiciels ne soient pas utiles, bien au contraire: ces outils rendent parfois énormément de service aux communautés scientifiques, notamment quand il s'agit de review de données et d'interopérabilité dans des communautés/disciplines scientifiques qui sont dominées par les BDD commerciales et logiciels propriétaires (exemple https://doi.org/10.21105/joss.02765).

Mais parce que cela signifie que les développements, sauf surprise, ont cessé et qu'il est encore plus difficile d'identifier ces logiciels. 


### Dans un monde idéal ... 
Idéalement, je voudrais pouvoir chercher ce code à l'aide d'un ou deux mots clés dans un moteur de recherche et surtout, accéder à une liste de résultats pertinente. 
Je voudrais pouvoir accéder aux codes des publications qui m'intéressent *via* leurs citations, ou encore mieux, depuis le DOI de l'article lié au logiciel. 

Je souhaiterais avoir une fenêtre de citation associée à la notice du logiciel ou script dans le catalogue avec un bouton pouvant générer une citation formatée de manière standardisée comme dans HAL.

Dans un monde idéal, je pourrais récupérer une liste pertinente de codes, voire filtrer les résultats selon des critères comme le langage de programmation ou la licence.

J'aimerais aussi que le système me propose des suggestions de codes et/ou de publications proches.

Dans le cadre d'une collaboration passée avec un industriel, j'ai beaucoup hésité entre l'utilisation de deux logiciels et j'aurais aimé connaître un peu leur histoire, et surtout le type de licence compatible avec l'entreprise concernée. Si en plus j'avais pu trouver l'information sur les contrats de suivi ou d'expertise via l'université, ce serait très utile. 

### Le recours aux outils de recherche généralistes 
Mais pour le moment, faute de connaître des outils qui me permettent tout cela, je fais une recherche Google. La liste de résultats est difficile à exploiter : il y a Dr X qui a fait tel truc pendant sa thèse, a produit un logiciel, je ne sais pas dans quelle université c'était et Google me renvoie plusieurs résultats... 
Quand je tente une autre requête, il n'y a pas de résultat car Dr X semble ne plus exercer dans le secteur académique.
GitHub me donne une liste assez basique. 

Une collègue de bonne volonté a voulu m'aider en me proposant des mots clés pour faire ma recherche :
le problème est que les mots clés pertinents pour elle, qui fait du développement, ne fonctionnent pas pour moi, simple utilisateur. Alors que je m'intéresse à l'objectif que permet d'atteindre le logiciel, ma collègue a besoin de s'intéresser à la méthode pour atteindre l'objectif. 


Au pire, je poserai ma question sur une liste de diffusion ou aux collègues. La méthode informelle est peut-être encore la plus efficace (quand les collègues se souviennent du nom exact du logiciel). 

### Citations et liens brisés
Récemment, des collègues m'ont montré comment utiliser un package bibLaTeX pour citer des logiciels. Je n'utilise pas ce type d'outils. J'ai plutôt tendance à mettre directement le lien vers la forge du logiciel. 

Cela dit, je me suis rendu compte que dans des articles pas si anciens que ça, ça avait créé des liens brisés. En fait, je vois un peu de tout en matière de citation et je ne suis pas suffisamment convaincu de la valeur ajoutée pour passer du temps dessus. 

Des collègues déposent leurs logiciels dans des plateformes d'archivage, je vais peut-être le faire aussi pour obtenir des DOI.

