#  Je contribue (ou contribuerais) à un logiciel existant plutôt que repartir de zéro

## Méthode
Ces *personas* sont des portraits robots d'utilisateurs fictifs mais réalistes.
Il ne s'agit donc pas de décrire des utilisateurs idéaux mais des situations réalistes.
Qu'est-ce qui peut encourager ou freiner l'utilisation d'un catalogue de logiciels? 

Le 1er enjeu est de se concentrer sur :
- **le type de tâches que doivent réaliser ces acteurs**: avec quels outils travaillent-ils ? avec quels autres acteurs ? y quelles sont les tâches récurrentes ? Quelles sont les tâches les plus importantes ? Quelles tâches souhaiteraient-ils réaliser différemment ? 
- **leurs questionnements** : quand le choix est possible, quels sont leurs critères de décision pour adopter un outil ? Quand il n'y a pas le choix, qui détermine l'outil à utiliser ? 


## Description de la situation
Je contribue à un autre projet de logiciel. Je n'en suis pas le principal porteur mais je participe au développement car je pourrais refaire un environnement complet et porter des applications, mais autant m'intégrer dans un logiciel exister et y contribuer. 

Ce ne sera donc pas "mon" logiciel, or je voudrais que ma contribution soit reconnue, affichée, pour apparaître dans mon évaluation même si le développement principal du logiciel reste dans le labo d'origine. 

Un autre problème est celui de mes contributions pour des projets, hors temps de travail. Pourtant, il s'agit de logiciels utilisés dans le monde académique. Mais comme cette activité n'entre dans aucune case, j'ai du mal à la valoriser. 
