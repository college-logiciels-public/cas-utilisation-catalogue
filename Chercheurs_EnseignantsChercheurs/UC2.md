# Je développe un code source ou un logiciel que je veux rendre visible

## Méthode
Ces *personas* sont des portraits robots d'utilisateurs fictifs mais réalistes.
Il ne s'agit donc pas de décrire des utilisateurs idéaux mais des situations réalistes.
Qu'est-ce qui peut encourager ou freiner l'utilisation d'un catalogue de logiciels? 

Le 1er enjeu est de se concentrer sur :
- **le type de tâches que doivent réaliser ces acteurs**: avec quels outils travaillent-ils ? avec quels autres acteurs ? y quelles sont les tâches récurrentes ? Quelles sont les tâches les plus importantes ? Quelles tâches souhaiteraient-ils réaliser différemment ? 
- **leurs questionnements** : quand le choix est possible, quels sont leurs critères de décision pour adopter un outil ? Quand il n'y a pas le choix, qui détermine l'outil à utiliser ? 


## Description de la situation

Avec les collègues, on a besoin de créer une communauté autour de notre logiciel car on doute de notre capacité à le maintenir à nous seul⋅es. 
On anticipe les besoins divers qui pourraient survenir : répondre et faire le suivi des propositions, concevoir et mettre à jour la documentation, etc. 

Nous prévoyons de prendre rdv avec le service communication de notre institution pour promouvoir notre logiciel, mais le temps manque et plusieurs collègues ont déjà fait remarquer que ça marche bien sur le coup, mais qu'il faut prévoir autre chose pour le temps long. Et se pose aussi la question de la création de communauté. Nous aurons besoin de gérer des interactions, de répartir des tâches et surtout, de veiller à garder une cohérence dans les développements. 

Nous avons participé à des conférences et même des salons, nous avons amélioré la visibilité de notre logiciel. 
Mais je ne peux pas y passer trop de temps. 

Dans la même idée, je veux mettre à disposition un prototype logiciel
que j'ai développé pour permettre de le tester.



