# Cas d'usage concernant les autres publics
Ajoutez les profils qui vous semblent concernés par la mise à disposition d'un catalogue logiciels. 

- [11: au sein du service informatique, je suis chargé⋅e du système d'information recherche](https://gricad-gitlab.univ-grenoble-alpes.fr/college-logiciels-public/cas-utilisation-catalogue/-/blob/main/Autres/UC11.md)
