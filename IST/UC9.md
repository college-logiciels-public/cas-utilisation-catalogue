# Je suis data steward à la BU

## Méthode
Ces *personas* sont des portraits robots d'utilisateurs fictifs mais réalistes.
Il ne s'agit donc pas de décrire des utilisateurs idéaux mais des situations réalistes.
Qu'est-ce qui peut encourager ou freiner l'utilisation d'un catalogue de logiciels? 

Le 1er enjeu est de se concentrer sur :
- **le type de tâches que doivent réaliser ces acteurs**: avec quels outils travaillent-ils ? avec quels autres acteurs ? quelles sont les tâches récurrentes ? Quelles sont les tâches les plus importantes ? Quelles tâches souhaiteraient-ils réaliser différemment ? 
- **leurs questionnements** : quand le choix est possible, quels sont leurs critères de décision pour adopter un outil ? Quand il n'y a pas le choix, qui détermine l'outil à utiliser ? 

## Description de la situation

Dans le cadre des évaluations, des directeur⋅trices d'équipe ont sollicité la BU pour qu'on fournisse des listes de logiciels. Nous avons l'habitude de traiter les autres types de documents, mais nous ne savons pas encore vraiment comment repérer la production logicielle d'un labo. 

On doit aussi s'assurer que des solutions ont été mises en place pour préserver ces ressources. Un peu comme ce qu'on fait pour les jeux de données. 
J'accompagne la mise en oeuvre de Data Management Plans et il y des sous-items dédiés au logiciel, mais nous n'avons pas un cadrage à donner sur le sujet. 
Idem, j'ai entendu parler de Software Management Plans, mais je crains que les chercheurs ne sautent pas de joie en découvrant ce nouveau type de document à compléter ... 

A la BU, nous promouvons activement les principes FAIR auprès des chercheurs. Je mène des actions de sensibilisation aux standards de métadonnées. Si ça marche pour les jeux de données, ça doit marcher pour les logiciels ? Pour le moment, avec les collègues, nous faisons surtout de l'auto-formation sur le sujet. Cela nous prend du temps et il n'est pas certain qu'on ait identifié tous les sujets auxquels porter attention. 

Lors des formations aux doctorants, de plus en plus de questions concernent les codes et logiciels développés dans le cadre de la thèse : doit-on les partager ? comment ? peuvent-ils être valorisés comme une publication ? quelles licences choisir ?

Par ailleurs, la direction de la BU nous pousse à proposer des formations à la citation de logiciel. Mais on dirait bien que c'est un peu le far west: pas de standards descriptifs comme nous en avons l'habitude pour les ouvrages ou d'autres types de documents pourtant exotiques.
La bibliothèque est aussi consultée pour élaborer la feuille de route "Science ouverte" de l'université. Je fournis des outils d'aide à la décision. (Verheul et al., 2019)

J'ai sympathisé avec des homologues dans d'autres établissements et nous nous rendons compte que nos missions ne sont pas standardisées. Nous avons un socle commun autour des principes FAIR, mais nos profils peuvent être très différents (Davidson, 2021) : par exemple, je ne fais pas de développement informatique à la différence d'une collègue d'une autre université, qui est statisticienne de formation. Une autre différence de taille : ma collègue est rattachée à une unité de recherche précise alors que ma mission est plutôt centralisée. 

Récemment, j'ai lu un rapport (Jetten et al., 2021) qui soulignait la nécessité d'identifier les complémentarités entre data stewards et research software engineers.* 

![](https://hedgedoc.softwareheritage.org/uploads/4e03805e-3211-4491-bd83-c3a5e374c27e.png)

Image tirée de: (Jetten et al., 2021)
