# Cas d'usage concernant les personnels de l'information scientifique et technique

- [9: je suis data steward à la BU](https://gricad-gitlab.univ-grenoble-alpes.fr/college-logiciels-public/cas-utilisation-catalogue/-/blob/main/IST/UC9.md?ref_type=heads)
- [10: je modère les dépôts dans HAL](https://gricad-gitlab.univ-grenoble-alpes.fr/college-logiciels-public/cas-utilisation-catalogue/-/blob/main/IST/UC10.md?ref_type=heads)
