# Cas d'usage concernant les directions d'unité

- [5 : je prépare une évaluation HCERES](https://gricad-gitlab.univ-grenoble-alpes.fr/college-logiciels-public/cas-utilisation-catalogue/-/blob/main/DU/UC5.md?ref_type=heads)
- [6: je veux afficher la production logicielle du laboratoire sur son site web](https://gricad-gitlab.univ-grenoble-alpes.fr/college-logiciels-public/cas-utilisation-catalogue/-/blob/main/DU/UC6.md?ref_type=heads)
