# Je veux afficher la production logicielle du laboratoire sur son site web

## Méthode
Ces *personas* sont des portraits robots d'utilisateurs fictifs mais réalistes.
Il ne s'agit donc pas de décrire des utilisateurs idéaux mais des situations réalistes.
Qu'est-ce qui peut encourager ou freiner l'utilisation d'un catalogue de logiciels? 

Le 1er enjeu est de se concentrer sur :
- **le type de tâches que doivent réaliser ces acteurs**: avec quels outils travaillent-ils ? avec quels autres acteurs ? y quelles sont les tâches récurrentes ? Quelles sont les tâches les plus importantes ? Quelles tâches souhaiteraient-ils réaliser différemment ? 
- **leurs questionnements** : quand le choix est possible, quels sont leurs critères de décision pour adopter un outil ? Quand il n'y a pas le choix, qui détermine l'outil à utiliser ? 


## Description de la situation
Pour les évaluations, je veux lister de manière exhaustive la production logicielle de mon
laboratoire. 
Mais je veux aussi que les contributions des membres de mon labo à des
logiciels open source apparaissent : elles sont difficiles à suivre. 

En fait, peut-être qu'une vitrine des logiciels serait plus adaptée qu'une liste complète. Quand c'est possible, nous mettons à disposition le code source des logiciels : les gens peuvent le télécharger depuis notre site. 
Mais dans de nombreux cas, on ne fait pas de mise à jour. 

