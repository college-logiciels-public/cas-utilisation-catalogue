# Cas d'usage : Préparation d'une évaluation HCERES

## Méthode
Ces *personas* sont des portraits robots d'utilisateurs fictifs mais réalistes.
Il ne s'agit donc pas de décrire des utilisateurs idéaux mais des situations réalistes.
Qu'est-ce qui peut encourager ou freiner l'utilisation d'un catalogue de logiciels? 

Le 1er enjeu est de se concentrer sur :
- **le type de tâches que doivent réaliser ces acteurs**: avec quels outils travaillent-ils ? avec quels autres acteurs ? y quelles sont les tâches récurrentes ? Quelles sont les tâches les plus importantes ? Quelles tâches souhaiteraient-ils réaliser différemment ? 
- **leurs questionnements** : quand le choix est possible, quels sont leurs critères de décision pour adopter un outil ? Quand il n'y a pas le choix, qui détermine l'outil à utiliser ? 


## Description de la situation

### Cas d'une unité de recherche produisant quelques logiciels
Au bout de plusieurs années, nous avons désormais des procédures bien rodées pour le recensement des publications. 

En revanche, nous ne nous sommes pas penchés sur la question du logiciel, à la différence d'autres structures où la production est plus massive. Certains homologues, pour qui la question est centrale, utilisent même des workflows qui permettent d'importer des informations issues d'une base de données interne.  
Pour nous, la question semblait moins critique mais j'ai perçu plusieurs signaux m'indiquant qu'il serait nécessaire de s'en emparer. 
Tout reste à faire.
Peut-être serait-il possible de dupliquer ce qui s'est fait pour les publications avec HAL? Je crois avoir vu passer une information sur le dépôt de logiciel dans HAL. Mais en posant la question à quelques collègues, on dirait que cette pratique n'est pas encore la norme. 

En plus, les collègues du service de valorisation me disent qu'ils ont développé leur propre base pour leur suivi. 

Peu m'importe l'outil, mon seul objectif est d'identifier comment tirer parti de ce recensement et de minimiser les efforts passés pour traiter ces informations. 


