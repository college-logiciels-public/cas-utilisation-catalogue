# J'alimente la base de données du service Valorisation


## Méthode
Ces *personas* sont des portraits robots d'utilisateurs fictifs mais réalistes.
Il ne s'agit donc pas de décrire des utilisateurs idéaux mais des situations réalistes.
Qu'est-ce qui peut encourager ou freiner l'utilisation d'un catalogue de logiciels? 

Le 1er enjeu est de se concentrer sur :
- **le type de tâches que doivent réaliser ces acteurs**: avec quels outils travaillent-ils ? avec quels autres acteurs ? y quelles sont les tâches récurrentes ? Quelles sont les tâches les plus importantes ? Quelles tâches souhaiteraient-ils réaliser différemment ? 
- **leurs questionnements** : quand le choix est possible, quels sont leurs critères de décision pour adopter un outil ? Quand il n'y a pas le choix, qui détermine l'outil à utiliser ? 


## Description de la situation

Dans le service, nous avons conçu une base de données interne pour tout recenser. Tous les champs prévus couvrent nos besoins. Nous avons notamment des champs pour les informations juridiques. Notre base met l'accent sur les questions de licence. 

Le problème est qu'à force d'ajouter des champs au fil des années, nous avons du mal à convaincre les chercheurs d'utiliser la base. Seule la question de l'évaluation les pousse à le faire.
Par ailleurs, nous savons que les logiciels sont aussi répertoriés dans d'autres bases mais nous n'avons pas d'appui informatique interne pour créer des passerelles entre outils. 

Pour les collègues, c'est assez prenant car ils font le tour des unités de recherche pour compléter les champs avec les chercheurs. C'est très riche en info, mais très accaparant. 

Et il est certain que des logiciels nous échappent. 
