# Je suis président⋅e d'organisme et je veux des indicateurs sur la production logicielle de mon établissement

## Méthode
Ces *personas* sont des portraits robots d'utilisateurs fictifs mais réalistes.
Il ne s'agit donc pas de décrire des utilisateurs idéaux mais des situations réalistes.
Qu'est-ce qui peut encourager ou freiner l'utilisation d'un catalogue de logiciels? 

Le 1er enjeu est de se concentrer sur :
- **le type de tâches que doivent réaliser ces acteurs**: avec quels outils travaillent-ils ? avec quels autres acteurs ? y quelles sont les tâches récurrentes ? Quelles sont les tâches les plus importantes ? Quelles tâches souhaiteraient-ils réaliser différemment ? 
- **leurs questionnements** : quand le choix est possible, quels sont leurs critères de décision pour adopter un outil ? Quand il n'y a pas le choix, qui détermine l'outil à utiliser ? 


## Description de la situation

Mes équipes me disent que la 1ère difficulté est d'obtenir une vision exhaustive de la production logicielle. Plusieurs services peuvent nous fournir des informations, mais elles sont parcellaires et tout cela reste assez lourd à compiler. 

De plus, plusieurs personnes m'ont interpellé sur la nécessité de prendre en compte les participations à des logiciels open source extérieurs à notre institution.

J'ai bien vu que le service communication avait mis en avant une personne qui contribue massivement à Matplotlib car la responsable du service avait eu vent de cette information de manière informelle. Mais c'est un objectif de communication : ce qui m'intéresse, ce sont les indicateurs, les recensements exhaustifs et systématisés.
J'en ai besoin pour répondre notamment aux demandes du ministère. Pas facile d'identifier les collaborations internationales autour du logiciel. Et quand on me demande une liste sur ce sujet, cela prend beaucoup de temps à produire actuellement. 

Je voudrais savoir dans quelle discipline et pour quel objectif de recherche les chercheurs de mon établissement développent ou participent au développement de codes, certes pour valoriser ces produits de la recherche mais aussi pour évaluer combien coûterait un soutien en termes RH. Il faut un retour sur investissement suffisant. 

Je veux aussi voir si ça peut alimenter la réflexion stratégique de notre établissement (les codes pourraient être pris en compte pour l'élaboration de la stratégie recherche ...)

Un type d'informations m'intéresse également pour du suivi : je voudrais des statistiques sur les licences utilisées. 

Recenser les logiciels est une première étape, mais en plus, il faudrait avoir une vue sur leurs statuts de versions (instables/stables). 

Autre préoccupation : comment valoriser les logiciels produits dans mon établissement de manière systématique/automatisée au niveau national ?
