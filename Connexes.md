# Cas d'usage connexes au catalogue

## Synthèse de bonnes pratiques et lieu d'échanges

### Description de la situation

Dans le cadre d'une prestation de service de développement sur un
logiciel de l'ESR, j'aimerais avoir des règles claires et
transparentes (à partir de mots-clefs issus d'une ontologie
structurée) pour garantir que les contributions soient faites dans le
respect des principes FAIR pour être correctement moissonnées et ainsi
répertoriées dans le catalogue. 


