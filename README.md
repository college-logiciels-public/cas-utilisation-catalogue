# Cas utilisation catalogue

Ce projet a pour objectif de permettre un enrichissement collectif des
cas d'usage d'un catalogue des logiciels de l'Enseignement supérieur
et de la recherche dans le cadre des travaux du [collège Codes Sources
et Logiciels du Comité pour la Science Ouverte](https://www.ouvrirlascience.fr/college-codes-sources-et-logiciels/).

Le dépôt se décompose en plusieurs répertoires pour structurer les cas
d'usage en fonction des missions de chacun :

- [Chercheurs et enseignants-
chercheurs](Chercheurs_EnseignantsChercheurs) : cas d'usage concernant
les chercheurs et les enseignants chercheurs, incluant les doctorants
et les post-doctorants
- [Ingénieurs et personnels d'appui à la recherche](Ingenieurs) : cas
  d'usage concernant l'ensemble des personnels d'appui à la recherche
  dans les laboratoires et les services techniques
- [Directeurs d'unité](DU) : cas d'usage concernant les directions de
  laboratoires et d'unité d'appui
- [Gouvernance des établissements](GouvernanceEtablissements) : cas
  d'usage concernant les présidences des établissements et organismes
  de recherche
- [Services de valorisation](ServicesValorisation) : cas d'usage
  concernant les services de valorisation de la recherche et les
  services de transfert
- [Information scientifique et technique](IST) : cas d'usage
  concernant les personnels de l'information scientifique et
  technique, en particulier les personnels des bibliothèques des établissements de
  l'enseignement supérieur et de la recherche
- [Autres](Autres) : cas d'usage concernant toutes les personnes en
  dehors des précédentes catégories, en particulier le grand public,
  les entreprises, les associations, ...

D'[autres cas d'usage connexes au catalogue](Connexes.md) sont également détaillés.
