# Vos retours sur les cas d'usage

## Informations de contact

- votre email: 

## Souhaitez-vous contribuer à des étapes ultérieures du travail ? 
Exemples d'étapes pour lesquelles vous pourriez être sollicité: relecture, ajout de compléments, etc. 
Dans tous les cas, il s'agirait d'actions ponctuelles. 

- [ ] oui 
- [ ] non
- [ ] ne sais pas

## Cas d'usage commenté

- 1a, 1b. "Je suis scientifique et je cherche un logiciel pour un besoin particulier (ou je suis ingénieur posté en unité de recherche)"
- 2a. "Je suis scientifique et je développe un logiciel que je veux rendre visible"
- 2b. "Je suis scientifique et je contribuerais volontiers à un logiciel existant plutôt que repartir de zéro"
- 3a, 3b. "Je suis directeur⋅trice de labo et je dois préparer mon évaluation HCERES / je veux lister de manière exhaustive la production logicielle de mon laboratoire"
- 3c. "Je suis président⋅e d'organisme et je veux des indicateurs sur la production logicielle de mon établissement"
- 3d. "Je suis chercheur⋅se/ingénieur⋅e et je dois remonter mes infos pour les indicateurs"
- 3e. "Je travaille dans un service de valorisation"
- 3f. "Au sein du service informatique, je suis chargé⋅e du système d'information recherche"
- 4a. "Je suis data steward. Je travaille à la BU auprès des équipes de recherche pour les aider à veiller à pérenniser et décrire leur production"
- 4b. "Je suis validateur⋅se de fiche HAL"

## Pour chaque cas d'usage commenté : quel type de tâches les personnes doivent-elles réaliser, en lien avec les logiciels? 
- Cas d'usage n° X: 
- Cas d'usage n° X: 
- Cas d'usage n° X: 

## Quelles sont les solutions les plus répandues pour répondre aux besoins des personnes décrites? 
- Cas d'usage n° X: 
- Cas d'usage n° X: 
- Cas d'usage n° X: 

## Quand cette ou ces personnes doivent prendre des décisions, qu'est-ce qui compte le plus pour elles? Et si cette ou ces personnes ne décident pas directement, d'où vient la décision? 
- Cas d'usage n° X: 
- Cas d'usage n° X: 
- Cas d'usage n° X: 

## Quelles sont les sources de frustration des personnes décrites ? 
- Cas d'usage n° X: 
- Cas d'usage n° X: 
- Cas d'usage n° X: 

## Quels sont les avantages majeurs des outils et processus actuellement utilisés par les personens décrites ? 
- Cas d'usage n° X: 
- Cas d'usage n° X: 
- Cas d'usage n° X: 

## Quels sont les profils d'utilisateurs à ajouter ? Qui pourrait être mobilisé dans le déploiement et la maintenance du catalogue ? Qui pourrait avoir besoin de ces services ? 
En un court paragraphe, indiquez la fonction et les missions de la personne. 


- Profil à ajouter: 

- Profil à ajouter: 
